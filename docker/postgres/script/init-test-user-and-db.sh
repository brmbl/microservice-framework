#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER $POSTGRES_TEST_USER WITH PASSWORD '$POSTGRES_TEST_PASSWORD';
	CREATE DATABASE $POSTGRES_TEST_DB;
	GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_TEST_DB TO $POSTGRES_TEST_USER;
	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_TEST_DB" <<-EOSQL
	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
EOSQL