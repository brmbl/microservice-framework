<?php

namespace App\Tests;

use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

class BaseWebTestCase extends WebTestCase
{
    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $dropSchemaCommand = new StringInput('doctrine:schema:drop --force --full-database -qn');
        $application->run($dropSchemaCommand);
        $applyMigrationCommand = new StringInput('doctrine:migration:migrate -qn');
        $application->run($applyMigrationCommand);
    }

    public function test_it_true(): void
    {
        $this->assertTrue(true);
    }
}