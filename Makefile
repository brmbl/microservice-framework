.PHONY: run-tests, install, up, php, db

install: ##Bringing up the containers, run composer install and apply migrations
	docker-compose up -d
	docker-compose exec php bash -c "composer install"
	docker-compose exec php bash -c "bin/console doctrine:migration:migrate -qn"
up: ##Bringing up the containers
	docker-compose up -d
php: ##Execute php container bash
	docker-compose exec php bash
run-tests: ##Run`s the unit tests
	docker-compose exec php bash -c "php ./vendor/phpunit/phpunit/phpunit tests"
db:	##Recreate database for dev and test environments
	docker-compose exec php bash -c "bin/console doctrine:schema:drop --force --full-database -qn --env=dev"
	docker-compose exec php bash -c "bin/console doctrine:schema:drop --force --full-database -qn --env=test"
	docker-compose exec php bash -c "bin/console doctrine:migration:migrate -qn --env=dev"
	docker-compose exec php bash -c "bin/console doctrine:migration:migrate -qn --env=test"









help:
		@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
		}' $(MAKEFILE_LIST)
.DEFAULT_GOAL=help