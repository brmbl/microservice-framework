<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function checkHealth(): JsonResponse
    {
        return new JsonResponse(['success' => true]);
    }
}